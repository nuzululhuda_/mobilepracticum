<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel = DB::table('artikel')
            ->join('pengguna', 'artikel.user_id', '=', 'pengguna.id')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->select(
                'artikel.id', 'kategori.kategori', 'pengguna.nama', 'artikel.content', 
                'artikel.status', 'artikel.judul', 'artikel.user_id'
            )
            ->get();
        return response()->json($artikel, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'kategori_id' => 'required',
            'content' => 'required',
            'status' => 'required',
            'judul' => 'required'
        ]);
        
        DB::table('artikel')->insert([
            'user_id' => $request->user_id,
            'kategori_id' => $request->kategori_id,
            'content' => $request->content,
            'status' => $request->status,
            'judul' =>  $request->judul
        ]);
        
        return response()->json([
            'success' => true,
            'data' => [
                'user_id' => $request->user_id,
                'kategori_id' => $request->kategori_id,
                'content' => $request->content,
                'status' => $request->status,
                'judul' => $request->judul,
            ],
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artikel = DB::table('artikel')
            ->join('pengguna', 'artikel.user_id', '=', 'pengguna.id')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->select(
                'artikel.id', 'kategori.kategori', 'pengguna.nama', 'artikel.content', 
                'artikel.status', 'artikel.judul', 'artikel.user_id'
                )
            ->where('artikel.id', $id)
            ->first(); 
        return response()->json($artikel, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {       
            $artikel = Artikel::where('id', $id)->firstOrFail();
            $request->validate([
                'user_id' => 'required',
                'kategori_id' => 'required',
                'content' => 'required',
                'status' => 'required',
                'judul' => 'required'
            ]);
            
            DB::table('artikel')
                ->where('id', $id)
                ->update([
                    'user_id' => $request->user_id,
                    'kategori_id' => $request->kategori_id,
                    'content' => $request->content,
                    'status' => $request->status,
                    'judul' => $request->judul,
                ]);
            
            return response()->json([
                'success' => true,
                'data' => [
                    'user_id' => $request->user_id,
                    'kategori_id' => $request->kategori_id,
                    'content' => $request->content,
                    'status' => $request->status,
                    'judul' => $request->judul,
                ],
            ], 200);
    
           
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $artikel = Artikel::where('id', $id)->firstOrFail();
            $artikel->delete();
            return response()->json([
                'success' => true,
            ], 200);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ], 404);
        }
    }
}
