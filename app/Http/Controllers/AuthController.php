<?php

namespace App\Http\Controllers;

use Validator;
use App\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
       
        $request->validate([
            'email' => 'required|unique:pengguna|email',
            'nama' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'jenis_kelamin' => 'required',
            'password' => 'required',
            'pekerjaan' => 'required',
        ]);

        $data = [
            'email' => $request->email,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'password' => Hash::make($request->password),
            'api_token' => str_random(50),
            'pekerjaan' => $request->pekerjaan,
        ]; 
        
        Pengguna::create($data);
        $response = [
            'success' => true,
            'user_registered' => $data,
        ];
        return response()->json($response, 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $email = $request->email;
        $password = $request->password;

        $pengguna = Pengguna::where('email', $email)->first();

        if ($pengguna){
            if (Hash::check($request->password, $pengguna->password)){
                $pengguna->api_token = str_random(50);
                $pengguna->update();

                $response = [
                    'success' => true,
                    'user' => $pengguna,
                ];
                $statusCode = 200;
            }else {
                $response = [
                    'success' => true,
                    'message' => 'Invalid password',
                ];
                $statusCode = 401;
            }
        }else {
            $response = [
                'success' => true,
                'message' => 'Invalid credential',
            ];
            $statusCode = 401;
        }
        
        return response()->json($response, $statusCode);
    }

    public function logout(Request $request)
    {
        
        $api_token = $request->get('api_token');
       
        $user = Pengguna::where('api_token', $api_token)->first();
        if ($user){
            $user->api_token = null;
            $user->save();
            $response = [
                'success' => true,
                'message' => 'Logout success'
            ];
            $statusCode = 200;
        }else {
            $response = [
                'success' => false,
                'message' => 'User not found'
            ];
            $statusCode = 404;
        }

        return response()->json($response, $statusCode);
    }
}
